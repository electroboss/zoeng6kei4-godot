extends Node2D


const piecenames = [
	"紅兵",
	"紅俥",
	"紅傌",
	"紅相",
	"紅仕",
	"紅帥",
	"紅炮",
	"黑卒",
	"黑車",
	"黑馬",
	"黑象",
	"黑士",
	"黑將",
	"黑砲"
]
onready var piece_template = load("res://PieceTemplate.tscn")
onready var possibdot_template = load("res://PossibDotTemplate.tscn")
var pieces = []
var boardstate = [
	[2,3,4,5,6,5,4,3,2],
	[0,0,0,0,0,0,0,0,0],
	[0,7,0,0,0,0,0,7,0],
	[1,0,1,0,1,0,1,0,1],
	[0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0],
	[8,0,8,0,8,0,8,0,8],
	[0,14,0,0,0,0,0,14,0],
	[0,0,0,0,0,0,0,0,0],
	[9,10,11,12,13,12,11,10,9],
]
var activeplayer = 0
var history = []


# Called when the node enters the scene tree for the first time.
func _ready():
	updateboard(boardstate, null, activeplayer)


func create_piece(piece, position, flipped, intpos=null):
	var newpiece = piece_template.instance()
	newpiece.get_child(0).texture = load("res://piece_images/"+piece+".png")
	newpiece.get_child(0).flip_v = flipped
	newpiece.get_child(0).flip_h = flipped
	newpiece.position[0] = position[0]*120
	newpiece.position[1] = position[1]*120
	if intpos != null:
		newpiece.get_child(1).connect("pressed",self,"piecepressed",intpos)
	else:
		newpiece.get_child(1).connect("pressed",self,"piecepressed",[position[0],position[1]])
	return newpiece

func create_possib_dot(position, activepiecepos, intpos=null):
	var possibdot = possibdot_template.instance()
	possibdot.position[0] = position[0]*120
	possibdot.position[1] = position[1]*120
	if intpos != null:
		possibdot.get_child(1).connect("pressed",self,"movepressed",[intpos[0],intpos[1],activepiecepos[0],activepiecepos[1]])
	else:
		possibdot.get_child(1).connect("pressed",self,"movepressed",[position[0],position[1],activepiecepos[0],activepiecepos[1]])
	return possibdot


func piecepressed(i,j):
	if colourof(getpiece(boardstate,i,j)) == activeplayer:
		updateboard(boardstate, [i,j], activeplayer)
	else:
		clear_piece()

func movepressed(i,j,x,y): # dot pos then piece pos
	activeplayer = 1-activeplayer
	addhistory(boardstate)
	boardstate[j][i] = boardstate[y][x]
	boardstate[y][x] = 0
	updateboard(boardstate, null, activeplayer)


func addhistory(boardstate):
	var newboardstate = []
	for x in boardstate:
		var q = []
		for y in x:
			q.append(y)
		newboardstate.append(q)
	history.append(newboardstate)


func colourof(piece):
	if piece <= 0:
		return -1
	return int(piece > 7)

func getpiece(boardstate, i, j):
	if i < 0 or i >= 9 or j < 0 or j >= 10:
		return -1
	else:
		return boardstate[j][i]


func getpossibmoves(i,j,boardstate):
	var possibmoves = []
	## TEMP
	#for x in range(9):
	#	for y in range(10):
	#		possibmoves.append([x,y])
	#return possibmoves
	## ENDTEMP
	var piece = getpiece(boardstate,i,j)
	if piece == 0:
		return possibmoves
	var colour = colourof(getpiece(boardstate,i,j))
	var oppcolour = 1-colour
	
	# zeot1
	if piece == 1:
		possibmoves.append([i,j+1])
		if j >= 5:
			possibmoves.append([i+1,j])
			possibmoves.append([i-1,j])
	if piece == 8:
		possibmoves.append([i,j-1])
		if j <= 4:
			possibmoves.append([i+1,j])
			possibmoves.append([i-1,j])
	
	# ce1
	if piece == 2 or piece == 9:
		possibmoves += possiborthmoves(i,j,boardstate, true)
	
	# zoeng6
	if piece == 4 or piece == 7+4:
		if getpiece(boardstate,i-1,j-1) == 0:
			if getpiece(boardstate,i-2,j-2) == 0 or colourof(getpiece(boardstate,i-2,j-2)) == oppcolour:
				if colour == 0 and j-2 < 5: # riverbed
					possibmoves.append([i-2,j-2])
				if colour == 1 and j-2 > 4: # riverbed
					possibmoves.append([i-2,j-2])
		
		if getpiece(boardstate,i+1,j-1) == 0:
			if getpiece(boardstate,i+2,j-2) == 0 or colourof(getpiece(boardstate,i+2,j-2)) == oppcolour:
				if colour == 0 and j-2 < 5: # riverbed
					possibmoves.append([i+2,j-2])
				if colour == 1 and j-2 > 4: # riverbed
					possibmoves.append([i+2,j-2])
		
		if getpiece(boardstate,i-1,j+1) == 0:
			if getpiece(boardstate,i-2,j+2) == 0 or colourof(getpiece(boardstate,i-2,j+2)) == oppcolour:
				if colour == 0 and j+2 < 5: # riverbed
					possibmoves.append([i-2,j+2])
				if colour == 1 and j+2 > 4: # riverbed
					possibmoves.append([i-2,j+2])
		
		if getpiece(boardstate,i+1,j+1) == 0:
			if getpiece(boardstate,i+2,j+2) == 0 or colourof(getpiece(boardstate,i+2,j+2)) == oppcolour:
				if colour == 0 and j+2 < 5: # riverbed
					possibmoves.append([i+2,j+2])
				if colour == 1 and j+2 > 4: # riverbed
					possibmoves.append([i+2,j+2])
	
	# maa5
	if piece == 3 or piece == 7+3:
		if getpiece(boardstate,i,j-1) == 0:
			if getpiece(boardstate,i-1,j-2) == 0 or colourof(getpiece(boardstate,i-1,j-2)) == oppcolour:
				possibmoves.append([i-1,j-2])
			if getpiece(boardstate,i+1,j-2) == 0 or colourof(getpiece(boardstate,i+1,j-2)) == oppcolour:
				possibmoves.append([i+1,j-2])
		
		if getpiece(boardstate,i,j+1) == 0:
			if getpiece(boardstate,i-1,j+2) == 0 or colourof(getpiece(boardstate,i-1,j+2)) == oppcolour:
				possibmoves.append([i-1,j+2])
			if getpiece(boardstate,i+1,j+2) == 0 or colourof(getpiece(boardstate,i+1,j+2)) == oppcolour:
				possibmoves.append([i+1,j+2])
		
		if getpiece(boardstate,i-1,j) == 0:
			if getpiece(boardstate,i-2,j+1) == 0 or colourof(getpiece(boardstate,i-2,j+1)) == oppcolour:
				possibmoves.append([i-2,j+1])
			if getpiece(boardstate,i-2,j-1) == 0 or colourof(getpiece(boardstate,i-2,j-1)) == oppcolour:
				possibmoves.append([i-2,j-1])
		
		if getpiece(boardstate,i+1,j) == 0:
			if getpiece(boardstate,i+2,j+1) == 0 or colourof(getpiece(boardstate,i+2,j+1)) == oppcolour:
				possibmoves.append([i+2,j+1])
			if getpiece(boardstate,i+2,j-1) == 0 or colourof(getpiece(boardstate,i+2,j-1)) == oppcolour:
				possibmoves.append([i+2,j-1])
	
	# paau3
	if piece == 7 or piece == 7+7:
		possibmoves += possiborthmoves(i,j,boardstate,false)
		# skip one then take
		var b = 0
		for x in range(i+1,9,1):
			if getpiece(boardstate,x,j) != 0:
				b += 1
			if b == 2 and colourof(getpiece(boardstate,x,j)) == oppcolour:
				possibmoves.append([x,j])
		b = 0
		for x in range(i-1,-1,-1):
			if getpiece(boardstate,x,j) != 0:
				b += 1
			if b == 2 and colourof(getpiece(boardstate,x,j)) == oppcolour:
				possibmoves.append([x,j])
		b = 0
		for x in range(j+1,10,1):
			if getpiece(boardstate,i,x) != 0:
				b += 1
			if b == 2 and colourof(getpiece(boardstate,i,x)) == oppcolour:
				possibmoves.append([i,x])
		b = 0
		for x in range(j-1,-1,-1):
			if getpiece(boardstate,i,x) != 0:
				b += 1
			if b == 2 and colourof(getpiece(boardstate,i,x)) == oppcolour:
				possibmoves.append([i,x])
	
	# si6
	if piece == 5 or piece == 7+5:
		if i != 4:
			if colourof(getpiece(boardstate,4,colour*7+1)) != colour:
				possibmoves.append([4,colour*7+1])
		else:
			if colourof(getpiece(boardstate,5,colour*7+0)) != colour:
				possibmoves.append([5,colour*7+0])
			if colourof(getpiece(boardstate,5,colour*7+2)) != colour:
				possibmoves.append([5,colour*7+2])
			if colourof(getpiece(boardstate,3,colour*7+0)) != colour:
				possibmoves.append([3,colour*7+0])
			if colourof(getpiece(boardstate,3,colour*7+2)) != colour:
				possibmoves.append([3,colour*7+2])
	
	# zoeng3
	if piece == 6 or piece == 7+6:
		var possibmovestemp = [[i+1,j],[i-1,j],[i,j-1],[i,j+1]]
		for possib in possibmovestemp:
			if getpiece(boardstate,possib[0],possib[1]) != -1:
				if (3 <= possib[0] and possib[0] <= 5) and (colour*7+0 <= possib[1] and possib[1] <= colour*7+2):
					if colourof(getpiece(boardstate,possib[0],possib[1])) != colour:
						# check for other general
						var a = false
						if colour == 0:
							for y in range(possib[1]+1,10):
								var q = getpiece(boardstate,possib[0],y)
								if q == 13:
									a = true
									break
								if q != 0:
									break
						if colour == 1:
							for y in range(possib[1]-1,-1,-1):
								var q = getpiece(boardstate,possib[0],y)
								if q == 6:
									a = true
								if q != 0:
									break
						if not a:
							possibmoves.append(possib)
	
	
	return possibmoves

func possiborthmoves(i,j,boardstate,cantakeend):
	var possibmoves = []
	var colour = colourof(getpiece(boardstate,i,j))
	var oppcolour = 1-colour
	
	for x in range(i+1,9):
		if getpiece(boardstate,x,j) != 0:
			if colourof(getpiece(boardstate,x,j)) == oppcolour and cantakeend:
				possibmoves.append([x,j])
			break
		possibmoves.append([x,j])
	
	for x in range(i-1,-1,-1):
		if getpiece(boardstate,x,j) != 0:
			if colourof(getpiece(boardstate,x,j)) == oppcolour and cantakeend:
				possibmoves.append([x,j])
			break
		possibmoves.append([x,j])
	
	for y in range(j+1,10):
		if getpiece(boardstate,i,y) != 0:
			if colourof(getpiece(boardstate,i,y)) == oppcolour and cantakeend:
				possibmoves.append([i,y])
			break
		possibmoves.append([i,y])
	
	for y in range(j-1,-1,-1):
		if getpiece(boardstate,i,y) != 0:
			if colourof(getpiece(boardstate,i,y)) == oppcolour and cantakeend:
				possibmoves.append([i,y])
			break
		possibmoves.append([i,y])
	
	return possibmoves


func updateboard(boardstate, activepiecepos, activeplayer):
	var possibmoves = []
	if activepiecepos != null:
		possibmoves = getpossibmoves(activepiecepos[0],activepiecepos[1],boardstate)
	pieces = []
	
	for piece in $Pieces.get_children():
		$Pieces.remove_child(piece)
		piece.queue_free()
	
	var playerfocus = 0
	if Global.fliporstill == 0:
		playerfocus = activeplayer
	
	# could be tidied up
	for i in range(9):
		for j in range(10):
			var pieceloc = [i,j]
			if playerfocus == 0:
				pieceloc = [i,9-j]
			var piece = getpiece(boardstate,i,j)
			if piece != 0:
				pieces.append(create_piece(piecenames[piece-1],[pieceloc[0],pieceloc[1]],(colourof(piece) != playerfocus) and Global.fliporstill,[i,j]))
			if [i,j] in possibmoves:
				pieces.append(create_possib_dot([pieceloc[0],pieceloc[1]],activepiecepos, [i,j]))
			j += 1
		i += 1
	
	for piece in pieces:
		$Pieces.add_child(piece)
	
	if activepiecepos == null:
		$SelectedPiece.position[0] = -120
	else:
		$SelectedPiece.position[0] = activepiecepos[0]*120
		if playerfocus == 0:
			$SelectedPiece.position[1] = (9-activepiecepos[1])*120+360 # 360 for space between top of board and top of screen
		else:
			$SelectedPiece.position[1] = activepiecepos[1]*120+360 # 360 for space between top of board and top of screen
	
	$PlayerIndicator1.changebanner(activeplayer)
	$PlayerIndicator2.changebanner(activeplayer)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func clear_piece():
	updateboard(boardstate, null, activeplayer)


func undo():
	if history == []:
		return
	boardstate = history.pop_back()
	activeplayer = 1-activeplayer
	updateboard(boardstate, null, activeplayer)
