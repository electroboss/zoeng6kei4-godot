extends TextureRect


var red_banner = load("RedBanner.png")
var black_banner = load("BlackBanner.png")


# Called when the node enters the scene tree for the first time.
func _ready():
	changebanner(1)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func changebanner(banner):
	if banner == 1:
		self.texture = black_banner
	if banner == 0:
		self.texture = red_banner
